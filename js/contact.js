"use strict";

window.onload = function (){
    setUpForm();
}

function setUpForm() {
    const form = document.getElementById('form');
    form.onsubmit = sendData;
}

function sendData(event) {
    const userName = document.getElementById('name').value;
    const message = document.getElementById('form-message').value;
    const title = document.getElementById('title').value;

    event.preventDefault();
    window.open('mailto:tatarualexandru777@gmail.com?subject='+ title+'. From '+userName+'&body='+message);
}

function checkFormValidity() {
    const isValid = document.getElementById('form').checkValidity();
    document.getElementById('submitButton').disabled = !isValid;
}