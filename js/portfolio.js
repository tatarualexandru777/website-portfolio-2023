"use strict";

window.onload = function (){
    loadDefaultPortfolioSection();
}

function loadDefaultPortfolioSection() {
    const images = document.getElementsByClassName("page-body")[0].children;
    for (let i = 0; i < images.length; i++) {
        images[i].style.display = images[i].className === "digital-art" ? "block" : "none";
    }
}

function changeNavigationItemStyle(element) {
    const navigationItems = element.parentElement.children;

    for(let i = 0; i < navigationItems.length; i++){
        navigationItems[i].className = "navigation-item";
    }
    element.className = "text-accent";
}

function onFilterChanged(index, element) {
    const images = document.getElementsByClassName("page-body")[0].children;
    const imagesContainer = document.getElementsByClassName("page-body")[0];

    imagesContainer.style.gridTemplateColumns = "repeat(4, auto)";
    imagesContainer.scrollTo(0, 0);

    changeNavigationItemStyle(element);

    for (let i = 0; i < images.length; i++) {
        switch (index) {
            case 0 :
                images[i].style.display = images[i].className === "digital-art" ? "block" : "none";
                break;
            case 1 :
                images[i].style.display = images[i].className === "software" ? "block" : "none";
                imagesContainer.style.gridTemplateColumns = "unset";
                imagesContainer.style.gap = "40px"
                break;
            case 2 :
                images[i].style.display = images[i].className === "3d-modelling" ? "block" : "none";
                imagesContainer.style.gridTemplateColumns = "repeat(3, auto)";
                break;
            default :
                return;
        }
    }
}